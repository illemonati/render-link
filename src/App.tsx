import React, { ChangeEvent, useEffect, useState } from "react";
import "./App.css";
import {
    Button,
    Container,
    FormControlLabel,
    Switch,
    TextField,
} from "@material-ui/core";
import { minify } from "terser";

const App = () => {
    const [location, setLocation] = useState("");
    const [javascriptConvertMode, setJavascriptConvertMode] = useState(false);
    const [output, setOutput] = useState("");

    const handleLocationChange = (e: ChangeEvent<HTMLInputElement>) => {
        setLocation(() => e.target.value);
    };

    useEffect(() => {
        const getHrefOutput = async () => {
            if (!javascriptConvertMode) return location;
            try {
                const minified = await minify(location, {
                    sourceMap: false,
                    compress: {
                        arrows: true,
                        arguments: true,
                        booleans: true,
                        collapse_vars: true,
                        computed_props: true,
                        dead_code: true,
                        drop_debugger: false,
                        evaluate: true,
                        inline: true,
                        join_vars: true,
                    },
                });
                const uri = encodeURIComponent(minified.code || "");
                return `javascript:${uri}`;
            } catch (e) {
                return "";
            }
        };
        (async () => {
            const out = await getHrefOutput();
            setOutput(() => out);
        })();
    }, [location, javascriptConvertMode]);

    return (
        <div className="App">
            <Container maxWidth="md">
                <br />
                <br />

                <TextField
                    multiline
                    variant="outlined"
                    placeholder="location / javascript ..."
                    fullWidth
                    value={location}
                    onChange={handleLocationChange}
                    rows={10}
                />

                <br />
                <br />

                <FormControlLabel
                    control={
                        <Switch
                            checked={javascriptConvertMode}
                            onChange={(e) =>
                                setJavascriptConvertMode(e.target.checked)
                            }
                            color="secondary"
                        />
                    }
                    label="Javascript Conversion"
                />
                <br />
                <br />
                <Button variant="contained" color="primary" href={output}>
                    Output Link
                </Button>
            </Container>
        </div>
    );
};

export default App;
